package com.divine.benchmarktool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runners.Suite.SuiteClasses;

import com.divine.benchmarktool.CONSTANTS.Constants;

// tests @ build/reports/tests/test/index.html

@SuiteClasses(value = { Main.class })
public class UiTest
{
	private Constants words;
	private Main main;
	public UiTest() {
		main = new Main();
	}
	@Test 
	public void setURL() {
		main.setUrlTextField("http://google.com");
        assertEquals("http://google.com", main.getUrlTextField().getText());
    }
	@Test 
	public void setMultipleRequest() {
		main.setReqTextField("2");
		assertNotEquals("http://google.com", main.getRequestTextField().getText());
    }
	@Test 
	public void loadingWhenBtnClicked() throws InterruptedException {
		main.setUrlTextField("http://google.com");
		main.setReqTextField("2");
        main.getTestBtn().doClick();
        if (main.getStatusField().getText()!=null) {
        	assertEquals(words.LOAD, main.getStatusField().getText());
		}
        else
        {
        	assertNull(main.getStatusField().getText());
        }
    }
	@Test 
	public void loadingWhenBtnClicked2() {
		main.setUrlTextField("http://google.com");
		main.setReqTextField("2");
        main.getTestBtn().doClick();
        assertNotEquals(null, main.getStatusField().getText());
    }
}
