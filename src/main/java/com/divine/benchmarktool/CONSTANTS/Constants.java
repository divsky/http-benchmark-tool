package com.divine.benchmarktool.CONSTANTS;

public final class Constants
{
	public final String TITLE = "HTTP server benchmark tool";
	public final String CONFIG = "Configuration";
	public final String URL = "URL:";
	public final String URLMESSAGE = "Enter the URL of the server to be connected to";
	public final String REQ = "REQ:";
	public final String REQMESSAGE = "Enter the number of concurrent requests";
	public final String AVGREQ = "Average request time:";
	public final String TEST = "Test";
	public final String FAIL = "Failed request count:";
	public final String LOAD = "Loading, please wait";
	public final String READY = "Results Ready, click Results";
}
