package com.divine.benchmarktool.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Request extends Thread
{
	private final String url;
	private int serverResponse;
	private long responseTime;

	public Request(String url)
	{
		this.url = url;
	}
	public void run()
	{
		// connect to server and report
		try {
			
			responseTime = doWork(url);
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	// Calculates response time, connects to website
    private long doWork(String url) throws MalformedURLException, IOException
    {
    	URL server = new URL(url);
    	long startTime = System.currentTimeMillis();

    	HttpURLConnection connection = (HttpURLConnection) server.openConnection();

    	serverResponse = connection.getResponseCode();

    	BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    	while (in.readLine() != null) {}
    	in.close();

    	long elapsedTime = System.currentTimeMillis() - startTime;
    	return elapsedTime;
    }
	
	public int getServerResponse() {
		return serverResponse;
	}
	public long getResponseTime() {
		return responseTime;
	}
}
