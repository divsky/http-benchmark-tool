package com.divine.benchmarktool.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecService implements Runnable
{
	private final String URL;
	private final int requests;
	private static long avgResponse;
	private long sum;
	private int fails;

	public ExecService(int requests, String url)
	{
		this.requests = requests;
		URL = url;
	}

	@Override
	public void run() {
		ExecutorService tasks = Executors.newFixedThreadPool(requests);

		Request allReqs[] = new Request[requests];
		Request task;
		for (int i = 0; i < requests; i++)
		{
			task = new Request(URL);
			allReqs[i] = task;
			tasks.submit(task);
			task.run();
		}
		
		for (int i = 0; i < allReqs.length; i++)
		{
			try {
				allReqs[i].join();
				sum += allReqs[i].getResponseTime();
				if (allReqs[i].getServerResponse()!=200)
				{
					fails++;
				}
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		// calculate average response
		avgResponse = sum/allReqs.length;
	}

	public String getAvgResponse() {
		return String.valueOf(avgResponse);
	}

	public String getFails() {
		return String.valueOf(fails);
	}
}
