package com.divine.benchmarktool;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.divine.benchmarktool.CONSTANTS.Constants;
import com.divine.benchmarktool.core.ExecService;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Main {

	private JFrame main;
	private JTextField urlTextField;
	private JTextField reqTextField;
	private JTextField avgText;
	private JTextField failText;
	private JTabbedPane mainPane;
	private Constants word;
	static final int MY_MINIMUM = 0;
	static int MY_MAXIMUM = 1;
	private JTextField statusField;
	private static ExtentReports report;
	private JButton testBtn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.main.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		word = new Constants();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		main = new JFrame();
		main.setTitle(word.TITLE);
		main.setBounds(100, 100, 450, 300);
		main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		main.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		mainPane = new JTabbedPane(JTabbedPane.TOP);
		main.getContentPane().add(mainPane);
		
		configTab();
		resultsTab();
	}
	private void configTab()
	{
		JPanel configPanel = new JPanel();
		mainPane.addTab(word.CONFIG, null, configPanel, null);
		
		JLabel urlLabel = new JLabel(word.URL);
		urlLabel.setToolTipText(word.URLMESSAGE);
		
		JLabel reqLabel = new JLabel(word.REQ);
		reqLabel.setToolTipText(word.REQMESSAGE);
		
		urlTextField = new JTextField();
		urlTextField.setToolTipText(word.URLMESSAGE);
		urlTextField.setColumns(10);
		
		reqTextField = new JTextField();
		reqTextField.setToolTipText(word.REQMESSAGE);
		reqTextField.setColumns(10);
		
		statusField = new JTextField();
		statusField.setEditable(false);
		statusField.setColumns(10);
		
		testBtn = new JButton(word.TEST);
		testBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				clearResults();
				statusField.setText(word.LOAD);
				ExecService start = new ExecService(Integer.valueOf(reqTextField.getText()), urlTextField.getText());
				start.run();
				// set results
				setResults(start.getAvgResponse(), start.getFails());
				statusField.setText(word.READY);
			}

		});
		
		GroupLayout gl_configPanel = new GroupLayout(configPanel);
		gl_configPanel.setHorizontalGroup(
			gl_configPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_configPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_configPanel.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_configPanel.createSequentialGroup()
							.addComponent(urlLabel)
							.addGap(18)
							.addComponent(urlTextField))
						.addGroup(gl_configPanel.createSequentialGroup()
							.addComponent(reqLabel)
							.addGap(18)
							.addComponent(reqTextField, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(testBtn)
							.addGap(18)
							.addComponent(statusField, GroupLayout.PREFERRED_SIZE, 206, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(39, Short.MAX_VALUE))
		);
		gl_configPanel.setVerticalGroup(
			gl_configPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_configPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_configPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(urlLabel)
						.addComponent(urlTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_configPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(reqLabel)
						.addComponent(reqTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(testBtn)
						.addComponent(statusField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(168, Short.MAX_VALUE))
		);
		configPanel.setLayout(gl_configPanel);
	}

	private void resultsTab()
	{
		JPanel resultsPanel = new JPanel();
		mainPane.addTab("Results", null, resultsPanel, null);
		
		JLabel avgReqLabel = new JLabel(word.AVGREQ);
		
		JLabel failReqLabel = new JLabel(word.FAIL);
		
		avgText = new JTextField();
		avgText.setEditable(false);
		avgText.setColumns(10);
		
		failText = new JTextField();
		failText.setEditable(false);
		failText.setColumns(10);
		
		GroupLayout gl_resultsPanel = new GroupLayout(resultsPanel);
		gl_resultsPanel.setHorizontalGroup(
			gl_resultsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_resultsPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_resultsPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_resultsPanel.createSequentialGroup()
							.addComponent(avgReqLabel)
							.addGap(18)
							.addComponent(avgText, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_resultsPanel.createSequentialGroup()
							.addComponent(failReqLabel)
							.addGap(18)
							.addComponent(failText, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(227, Short.MAX_VALUE))
		);
		gl_resultsPanel.setVerticalGroup(
			gl_resultsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_resultsPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_resultsPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(avgReqLabel)
						.addComponent(avgText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_resultsPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(failReqLabel)
						.addComponent(failText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(171, Short.MAX_VALUE))
		);
		resultsPanel.setLayout(gl_resultsPanel);
	}
	
	public JTextField getUrlTextField() {
		return urlTextField;
	}

	public JTextField getRequestTextField() {
		return reqTextField;
	}
	
	public void setResults(String avg, String fail)
	{
		setAvgText(avg);
		setFailText(fail);
		report = new ExtentReports("build/reports/java/Results.html");
		ExtentTest test = report.startTest("Results from Benchmark");
		test.log(LogStatus.PASS, word.AVGREQ+" "+avg);
		test.log(LogStatus.INFO, word.FAIL+" "+fail);
		report.endTest(test);
		report.flush();
	}
	
	private void setAvgText(String avgText) {
		this.avgText.setText(avgText);
	}

	private void setFailText(String failText) {
		this.failText.setText(failText);
	}
	
	private void clearResults() {
		setAvgText("");
		setFailText("");
	}

	public JButton getTestBtn() {
		return testBtn;
	}

	public JTextField getStatusField() {
		return statusField;
	}

	public void setStatusField(String message) {
		this.statusField.setText(message);
	}
	
	public void setUrlTextField(String urlTextField) {
		this.urlTextField.setText(urlTextField);
	}

	public void setReqTextField(String requestTextField) {
		this.reqTextField.setText(requestTextField);
	}
}
